import React, { useState } from 'react';
import './App.css';
import MakeTask from '../components/makeTask';
import ListTasks from '../components/listTasks';
import { DragDropContext } from 'react-beautiful-dnd';
import Context from '../contex';
import { Droppable } from 'react-beautiful-dnd';

const styles = {
  content:{
    marginTop:"50px"
  }
}


function App() {

  let [tasks, setTask] = React.useState(
    [
      { id:0, copleted:false, title: "Заправить кровать"},
      { id:1, copleted:true, title: "Убраться в комнате"},
      { id:2, copleted:false, title: "Изучать react"},
      { id:3, copleted:false, title: "Выгулять собак"},
      { id:4, copleted:false, title: "Встретить маму после работы"},
    ]
  );

  let [typeStatus, setTypeStatus] = React.useState("All");

  function makeListItem(titleNewItem){
    if(titleNewItem != ''){
      if(tasks.length){
        setTask(
          tasks.concat([{
            id: Date.now()
            , copleted:false, title: titleNewItem
          }]
          )
        )
      }else{
        setTask(
          [{ id:0, copleted:false, title: titleNewItem}]
        )
      }
    }    
    
  }

  function deleteItem(taskIndex){
    
    setTask(
      tasks.filter( task => task.id != taskIndex) 
    )
    
  }



  return (
    <DragDropContext 
      onDragEnd={(result) => {
        let newTasksList = [...tasks];
        let moveItem = newTasksList[result.source.index]
        newTasksList.splice(result.source.index, 1);
        newTasksList.splice(result.destination.index, 0, moveItem);
        setTask ( newTasksList)
      }}
        > 
        <Context.Provider value = {{typeStatus, setTypeStatus, setTask, tasks, deleteItem}}>
          <div className="App">
            <h1>Список дел!</h1>
            <div className="content d-flex" style = {styles.content}>
              <div className="col-lg-5 makeNewTask">
                <MakeTask makeListItem={makeListItem}/>
              </div>
              
              <div className="col-lg-5 listTasks">

                <ListTasks 
                          tasks={tasks} >
                          </ListTasks>
                
              </div>
            </div>
          </div>
        </Context.Provider>
    </DragDropContext>  
  );
}

export default App;
