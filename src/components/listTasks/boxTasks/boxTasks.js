import React, {useContext} from 'react';
import './boxTasks.css';
import Task from './task';
import Context from '../../../contex';
import { Droppable } from 'react-beautiful-dnd';

function BoxTasks({ delItemFunc}){

   const {typeStatus, tasks} = useContext(Context);


   function renderTask(){
      
       if(typeStatus == "All"){
        return(
        <Droppable  
            droppableId={"colamun-1"}>
            
        { provided => (
            <div
                className="list-content"
                {...provided.droppableProps}
                ref={provided.innerRef}
                >
                {tasks.map((task, index)=>{
                     return(<Task title={ task.title } key={task.id} itemId={task.id} delItemFunc= {delItemFunc} complited={task.copleted} index={index}/> )
                })}
                {provided.placeholder}
            </div>
            
        )}
              </Droppable>
        )

       }else if(typeStatus == "Done"){
        return(
            <Droppable  
                droppableId={"colamun-1"}>    
                    { provided => (
                        <div
                            className="list-content"
                            {...provided.droppableProps}
                            ref={provided.innerRef}
                            >
                            {tasks.map((task, index)=>{
                                if(task.copleted === true){
                                    return( <Task title={ task.title } key={task.id} itemId={task.id} delItemFunc= {delItemFunc} complited={task.copleted} index={index}/> )
                                }    
                            })}
                            {provided.placeholder}
                        </div>            
                    )}
            </Droppable>
            )
       }else{

        return(
            <Droppable  
                droppableId={"colamun-1"}>
                
                { provided => (
                    <div
                        className="list-content"
                        {...provided.droppableProps}
                        ref={provided.innerRef}
                        >
                        {tasks.map((task, index)=>{
                            if(task.copleted === false){
                                return( <Task title={ task.title } key={task.id} itemId={task.id} delItemFunc= {delItemFunc} complited={task.copleted} index={index}/> )
                            }    
                        })}
                        {provided.placeholder}
                    </div>
                )}
            </Droppable>
        )
                
       }
       
   }
    return (
            <div className="boxTasksBody">
                    <h6 className="boxTitle">Ваши задания
                        </h6>
                <hr/>

                <ul>
                    
                        { 
                            renderTask()
                        }
                        
                        
                </ul>
            </div>
    )
}

export default BoxTasks;