import React, {useContext} from 'react';
import './task.css';
import Context from '../../../../contex';
import { Draggable } from 'react-beautiful-dnd';

function Task({title, itemId,  complited, index}){

    const {setTask, tasks, deleteItem} = useContext(Context);

    function setCheckedTask(indexItem, e){

        let classes = e.target.classList;
        
        console.log( Object.keys(classes).length);

        for( let i =0; i < Object.keys(classes).length; i++ ){
            if(classes[i] == "checkedTask"){
                delete classes[i];
            }else{
                e.target.classList.add("checkedTask");
            }
        }
        
        setTask(
            tasks.map( task=>{
                if (task.id == indexItem){
                    task.copleted = !task.copleted
                }
                return task;
            })
        );
        
         
    }

    function renderNeededIcon(){
        if(complited){
            return (<i className="far fa-check-square deleteTask checkIcon checkedTask"  onClick={(e) =>setCheckedTask(itemId, e)}></i>)
        }else{
            return (<i className="far fa-check-square deleteTask checkIcon"  onClick={(e) =>setCheckedTask(itemId, e)}></i>)
        }
        
        

        
    }

    return(
        <Draggable 
            draggableId={itemId.toString()}
            index={index} >
                { (provided) => (
                    
                    <div 
                        {...provided.draggableProps }
                        {...provided.dragHandleProps }
                        ref = { provided.innerRef }
                        className="taskBody"
                        >
                        <li>
                            {title}
                        </li>
                        <i className="fas fa-trash-alt deleteTask" onClick={deleteItem.bind(this, itemId)}></i>
                        
                        {renderNeededIcon()}
                    </div>
            
                )}
        </Draggable>    
    );
}

export default Task;