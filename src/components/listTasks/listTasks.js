import React from 'react';
import './listTasks.css';
import TypeTasks from './typesTasks';
import BoxTasks from './boxTasks';
import { Droppable } from 'react-beautiful-dnd';

function ListTasks({ tasks , delItemFunc}){
    return(
            <div className="listTasksBody ">
                <TypeTasks />
                <BoxTasks 
                />
            </div>  
            )}

export default ListTasks;
