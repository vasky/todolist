import React, {useContext} from "react";
import './typeTasks.css';
import Context from "../../../contex";

function TypeTasks(){

    const {setTypeStatus} = useContext(Context);

    function printPressedButton(e, button){
        setTypeStatus(button);
        let buttons = document.querySelectorAll(".buttonType");
        
        buttons.forEach(button => {
            button.className = "btn";

            if(button.id == e.target.id){
                button.classList.add("btn-primary");
                button.classList.add("buttonType");
            }else{
                button.classList.add("btn-outline-secondary");
                button.classList.add("buttonType");
            }
        });


    }
    return(
        <div className="TypeTasksBody">
                <button className="btn btn-primary buttonType" id="All" onClick={(e) => printPressedButton(e, "All")} >
                    Все задания
                    </button>
                <button className="btn btn-outline-secondary buttonType" id="Active" onClick={ (e) => printPressedButton(e, "Active")}>
                    Активные
                        </button>
                <button className="btn btn-outline-secondary buttonType" id="Done" onClick={ (e) => printPressedButton(e, "Done")}>
                    Выполненые
                        </button>
        </div>
    );
}

export default TypeTasks;