import React, {useState} from 'react';
import './makeTask.css';

function MakeTask({makeListItem}){

    let [inputTask, setTaskInput] = React.useState('');

    function changeInput(event){
        setTaskInput(event.target.value)
    }

    function submitInput(){
        makeListItem(inputTask);
        setTaskInput("")
    }
    return (
        <div className="makeTaskBody">

            <div className="inputDiv">
                <input type="text" value={inputTask} placeholder="Добавте новое задание " className='form-control inputMakeTask' onChange={changeInput}/>
            </div>
            
            <button className="btn btn-success d-flex justify-content-start" onClick = {submitInput}>
                Добавить задачу
                </button>
        </div>
    );
    
};

export default MakeTask;